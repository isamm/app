<?php
class Advert{

    private $conn;
    public $id;
    public $desc;
    public $titre;
    public $insert_at;
    public $update_at;
    public $id_user;
    public $price;
    public $id_categ;
    public $update_count;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function read(){
        $query = "SELECT * FROM annonces;";

        $result = $this->conn->prepare($query);
        $result->execute();

        return $result;
    }

    public function create(){
        $desc = $this->desc;
        $titre = $this->titre;
        $id_user = $this->id_user;
        $price = $this->price;
        $id_categ = $this->id_categ;

        $query = "INSERT INTO annonces (`id`, `desc`, `titre`, `insert_at`, `update_at`, `id_user`, `price`,`id_categ`, `update_count`) VALUES ('', '".$desc."', '".$titre."', now(), now(), $id_user, $price, $id_categ, 0);";


        $result = $this->conn->prepare($query);
        
        if ($result->execute() ){
            return true;
        }

        return false;
    }
}
?>