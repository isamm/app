<?php
header("content-Type:application/json");

include_once '../config/config.php';
include_once '../entity/advert.php';

$db = new Database();
$conn = $db->getConnection();
// test connextion

$adv = new Advert($conn);

// recuperation les données
$data = json_decode(file_get_contents("php://input"));


//preparation de l'objet advert
$adv->desc = $data->desc;
$adv->titre = $data->titre;
$adv->id_user = $data->id_user;
$adv->price = $data->price;
$adv->id_categ = $data->id_categ;

if ( $adv->create() ){
    // OK
    http_response_code(200);
    echo json_encode(array('message' => 'Advert sucessfully created'));
}
else{
    // erreur
    http_response_code(503);
    echo json_encode(array('message' => $adv));
}


?>