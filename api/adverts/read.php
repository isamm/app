<?php
header("content-Type:application/json");

include_once '../config/config.php';
include_once '../entity/advert.php';

$db = new Database();
$conn = $db->getConnection();
// test connextion

$adv = new Advert($conn);

$result = $adv->read();
$count = $result->rowCount();

if ($count == 0){
    http_response_code(404);
    echo json_encode(array('message' => 'not found!'));
}
else{
    $advert_arr = array();

    while($row = $result->fetch()){
        $element = array(
            "id" => $row["id"],
            "desc" => $row["desc"],
            "titre" => $row["titre"],
            "insert_at" => $row["insert_at"],
            "update_at" => $row["update_at"],
            "id_user" => $row["id_user"],
            "price" => $row["price"],
            "id_categ" => $row["id_categ"],
            "is_active" => $row["is_active"],
            "update_count" => $row["update_count"]
        );
        array_push($advert_arr, $element);
    }

    http_response_code(200);
    echo json_encode($advert_arr);
}
?>