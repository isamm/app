<?php
class Database{
    private $host = "localhost";
    private $pass = "";
    private $user = "root";
    private $db = "api";
    private $conn = "null";

    function getConnection(){
        try {
            $this->conn = new PDO("mysql:host=".$this->host.";dbname=". $this->db, $this->user,$this->pass);
        } catch (\Throwable $th) {
            //throw $th;
            echo "connection failed";
        }

        return $this->conn;
    }
}

?>